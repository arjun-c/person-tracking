from flask import Flask, request, jsonify, Response, render_template, url_for, redirect
import yolov3_deepsort as tracker
import argparse
from utils.parser import get_config
import requests
import validators
import json
import io
import cv2
import time
from multiprocessing import Process
import glob
import os

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('showresult.html')


def latst_video():
    list_of_files = glob.glob('/home/arjun/projects/person_tracking_app/django_app/content/media/videos/*')  # * means all if need specific format then *.csv
    latest_file = max(list_of_files, key=os.path.getctime)
    return latest_file


def gen():
    """Video streaming generator function."""
    video_url = latst_video()
    vc = cv2.VideoCapture(video_url)
    while True:
        read_return_code, frame = vc.read()
        encode_return_code, image_buffer = cv2.imencode('.jpg', frame)
        io_buf = io.BytesIO(image_buffer)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + io_buf.read() + b'\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/person_tracking')
def person_tracking():
    url = latst_video()
    print(url)
    parser = argparse.ArgumentParser()
    parser.add_argument("VIDEO_PATH", type=str)
    parser.add_argument("--config_detection", type=str, default="./configs/yolov3.yaml")
    parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
    parser.add_argument("--ignore_display", dest="display", action="store_false", default=True)
    parser.add_argument("--frame_interval", type=int, default=1)
    parser.add_argument("--display_width", type=int, default=800)
    parser.add_argument("--display_height", type=int, default=600)
    parser.add_argument("--save_path", type=str, default="./demo/demo.avi")
    parser.add_argument("--cpu", dest="use_cuda", action="store_false", default=True)
    parser.add_argument("--camera", action="store", dest="cam", type=int, default="-1")
    args = parser.parse_args([url])
    cfg = get_config()
    cfg.merge_from_file(args.config_detection)
    cfg.merge_from_file(args.config_deepsort)
    with tracker.VideoTracker(cfg, args) as vdo_trk:
        video = vdo_trk.run()


@app.route("/tracking/video", methods=['POST'])
def tracking():

    data = request.get_json()
    if 'url' in data and data['url'] != "":
        url = data['url']
    else:
        return jsonify(statusCode=404, statusMessage="Error! url is not accessible"), 404

    # if 'token' in data and data['token'] != "":
    #     token = data['token']
    # else:
    #     return jsonify(statusCode=404, statusMessage='Error: No Token found. '
    #                                                  'Please provide the token'), 404
    #
    if 'callbackUrl' in data and (validators.url(data['callbackUrl']) is True) or (data['callbackUrl']) != "":
        callback_url = data['callbackUrl']
    else:
        return jsonify(statusCode=404, statusMessage="Error found on callbackUrl. "
                                                     "Please provide a valid callbackUrl"), 404
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("VIDEO_PATH", type=str)
        parser.add_argument("--config_detection", type=str, default="./configs/yolov3.yaml")
        parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
        parser.add_argument("--ignore_display", dest="display", action="store_false", default=True)
        parser.add_argument("--frame_interval", type=int, default=1)
        parser.add_argument("--display_width", type=int, default=800)
        parser.add_argument("--display_height", type=int, default=600)
        parser.add_argument("--save_path", type=str, default="./demo/demo.avi")
        parser.add_argument("--cpu", dest="use_cuda", action="store_false", default=True)
        parser.add_argument("--camera", action="store", dest="cam", type=int, default="-1")
        args = parser.parse_args([url])
        cfg = get_config()
        cfg.merge_from_file(args.config_detection)
        cfg.merge_from_file(args.config_deepsort)
        with tracker.VideoTracker(cfg, args) as vdo_trk:
            track_details = vdo_trk.run()

        # response data
        data = {}
        all_tracks_list = []
        for key in track_details.keys():
            start_stop = []
            start_and_end_times = {'start': track_details[key][0], 'stop': track_details[key][1]}
            start_stop.append(start_and_end_times)
            all = {'id': str(key), 'track': start_stop}
            all_tracks_list.append(all)
        # data['token'] = token
        data['tracking'] = all_tracks_list
        status_final = json.dumps({'statusCode': 202, 'statusMessage': "Accepted"})
        final_data = json.dumps({'status': json.loads(status_final), 'data': data})
        requests.post(url=callback_url, json=final_data)
        return jsonify(statusCode=202, statusMessage="Accepted"), 202

    except Exception as exception:
        return jsonify(statusCode=500, statusMessage='Server Error. Please wait and try again!'), 500


if __name__ == "__main__":
    app.run(debug=True, host='127.0.0.1', port=8070)
