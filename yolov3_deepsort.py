import os
import cv2
import time
import argparse
import torch
import warnings
import numpy as np
from datetime import datetime
from PIL import Image
import json

from detector import build_detector
from deep_sort import build_tracker
from utils.draw import draw_boxes
from utils.parser import get_config
from main import gen

class VideoTracker(object):
    def __init__(self, cfg, args):
        self.cfg = cfg
        self.args = args
        self.save_path = '/home/arjun/projects/person_tracking_app/django_app/content/media/videos/demo.avi'
        use_cuda = args.use_cuda and torch.cuda.is_available()
        if not use_cuda:
            warnings.warn("Running in cpu mode which maybe very slow!", UserWarning)
        #
        # if args.display:
        #     cv2.namedWindow("test", cv2.WINDOW_NORMAL)
        #     cv2.resizeWindow("test", args.display_width, args.display_height)

        if args.cam != -1:
            print("Using webcam " + str(args.cam))
            self.vdo = cv2.VideoCapture(args.cam)
        else:
            self.vdo = cv2.VideoCapture()
        self.detector = build_detector(cfg, use_cuda=use_cuda)
        self.deepsort = build_tracker(cfg, use_cuda=use_cuda)
        self.class_names = self.detector.class_names


    def __enter__(self):
        if self.args.cam != -1:
            ret, frame = self.vdo.read()
            assert ret, "Error: Camera error"
            self.im_width = frame.shape[0]
            self.im_height = frame.shape[1]

        else:
            assert os.path.isfile(self.args.VIDEO_PATH), "Error: path error"
            self.vdo.open(self.args.VIDEO_PATH)
            self.im_width = int(self.vdo.get(cv2.CAP_PROP_FRAME_WIDTH))
            self.im_height = int(self.vdo.get(cv2.CAP_PROP_FRAME_HEIGHT))
            assert self.vdo.isOpened()

        if self.save_path:
            fourcc = cv2.VideoWriter_fourcc(*'MJPG')
            self.writer = cv2.VideoWriter(self.save_path, fourcc, 20, (self.im_width,self.im_height))

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            print(exc_type, exc_value, exc_traceback)

    def run(self):
        time_elapsed = {}
        idx_frame = 0
        while self.vdo.grab():
            idx_frame += 1
            if idx_frame % self.args.frame_interval:
                continue

            start = time.time()
            _, ori_im = self.vdo.retrieve()
            im = cv2.cvtColor(ori_im, cv2.COLOR_BGR2RGB)

            # do detection
            bbox_xywh, cls_conf, cls_ids = self.detector(im)
            if bbox_xywh is not None:
                # select person class
                mask = cls_ids==0

                bbox_xywh = bbox_xywh[mask]
                bbox_xywh[:,3:] *= 1.2 # bbox dilation just in case bbox too small
                cls_conf = cls_conf[mask]

                # do tracking
                outputs, error, frame = self.deepsort.update(bbox_xywh, cls_conf, im)
                if error:
                    exit(0)
                # initialises starting time of each id
                for i in outputs:
                    if i[4] not in time_elapsed.keys():
                        time_elapsed[i[4]] = []
                        time_elapsed[i[4]].append(datetime.now())
                        time_elapsed[i[4]].append(datetime.now())

                position = ((int)(ori_im.shape[1] / 2 - 268 / 2), (int)(ori_im.shape[0] / 2 - 36 / 2))
                # draw boxes for visualization
                if len(outputs) > 0:
                    if len(outputs) > 3:
                        cv2.putText(ori_im, 'crowd detected', position, cv2.FONT_HERSHEY_SIMPLEX, 2,
                                    (0,0,255), 5, cv2.LINE_AA)
                    bbox_xyxy = outputs[:,:4]
                    identities = outputs[:,-1]
                    ori_im, time_elapsed = draw_boxes(ori_im, time_elapsed, bbox_xyxy, identities)

            # ret, jpeg = cv2.imencode('.jpg', ori_im)
            # self.writer.write(ori_im)
            # return ori_im
            # cv2.imshow("test", ori_im)
            # shape = ori_im.shape
            # shared_array = np.Array(ctypes.c_uint16, shape[0] * shape[1] * shape[2], lock=False)
            # print(shared_array)
            # rgb = Image.fromarray(ori_im)
            # print(rgb)
            # r, g, b = cv2.split(ori_im)
            # img_bgr = cv2.merge([b, g, r])
            # ret, jpeg = cv2.imencode('.jpg', ori_im)
            # return jpeg.tobytes()
            # end = time.time()
            # print("time: {:.03f}s, fps: {:.03f}".format(end-start, 1/(end-start)))
            # if self.args.display:
            #     ret, jpeg = cv2.imencode('.jpg', ori_im)
            #     # cv2.imshow("test", ori_im)
            #     if cv2.waitKey(1) & 0xFF == ord('q'):
            #         break
            #     return jpeg.tobytes()
            #
            if self.save_path:
                print("-----")
                self.writer.write(ori_im)

        # dump time elapsed for each id
        time_elapsed_original = {}
        for key in time_elapsed:
            time_elapsed_original.update({str(key): (time_elapsed[key][1]-time_elapsed[key][0]).seconds})
        with open('time_elapse.json', 'w+') as fp:
            json.dump(time_elapsed_original, fp)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("VIDEO_PATH", type=str)
    parser.add_argument("--config_detection", type=str, default="./configs/yolov3.yaml")
    parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
    parser.add_argument("--ignore_display", dest="display", action="store_false", default=True)
    parser.add_argument("--frame_interval", type=int, default=25)
    parser.add_argument("--display_width", type=int, default=800)
    parser.add_argument("--display_height", type=int, default=600)
    parser.add_argument("--save_path", type=str, default="./demo/demo.avi")
    parser.add_argument("--cpu", dest="use_cuda", action="store_false", default=True)
    parser.add_argument("--camera", action="store", dest="cam", type=int, default="-1")
    return parser.parse_args()


if __name__=="__main__":
    args = parse_args()
    cfg = get_config()
    cfg.merge_from_file(args.config_detection)
    cfg.merge_from_file(args.config_deepsort)

    with VideoTracker(cfg, args) as vdo_trk:
        vdo_trk.run()
